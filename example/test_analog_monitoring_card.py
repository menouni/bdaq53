# ------------------------------------------------------------
# BDAQ53: Simple hardware test
# Basic DAQ hardware and chip configuration
#
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import yaml
from bdaq53.system.bdaq53 import BDAQ53
from bdaq53.system.analog_monitoring_board import MonitoringBoard

rx_lanes = 1


# Initialization
with open('..' + os.sep + 'bdaq53' + os.sep + 'system' + os.sep + 'bdaq53.yaml', 'r') as f:
    cnfg = yaml.full_load(f)

bdaq = BDAQ53(cnfg)
bdaq.init()

ana_board = MonitoringBoard(bdaq['i2c'])
ana_board.init()

bdaq.close()
