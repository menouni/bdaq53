#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This minimal example script shows how to manipulate RD53A registers and read analog data with the internal ADC.
'''

from bdaq53.system.bdaq53 import BDAQ53
from bdaq53.chips.rd53a import RD53A

# Init BDAQ anc chip objects
bdaq = BDAQ53()
bdaq.init()

# Read BDAQ FPGA temperature
print('BDAQ FPGA temperature = {0:1.2f}°C'.format(bdaq.get_temperature_FPGA()))

chip = RD53A(bdaq, recevier='rx0')
chip.init()

# Read a register
print('VCAL_HIGH = ', chip.registers['VCAL_HIGH'].read())

# Write a register
chip.registers['VCAL_HIGH'].write(value=1000)
print('VCAL_HIGH = ', chip.registers['VCAL_HIGH'].read())

# Read a value from internal ADC
print('VREF_A = {0}V'.format(chip.get_ADC_value('VREF_Ana_SLDO')[1]))

# Read on-chip temperature sensors
chip.get_temperature_sensors()
# ... and NTC
chip.get_temperature_NTC()

# Read all ring oscillators
print(chip.get_ring_oscillators())

# Read all possible ADC values
print(chip.get_chip_status())
