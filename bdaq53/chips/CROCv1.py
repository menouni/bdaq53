#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import yaml
import os

from bdaq53.chips.chip_base import RegisterObject
from bdaq53.chips.ITkPixV1 import ITkPixV1, ITkPixV1MaskObject, ITkPixV1Calibration
from bdaq53.system import logger as logger

FLAVOR_COLS = {'LIN': range(0, 432)}


def get_flavor(col):
    for fe, cols in FLAVOR_COLS.items():
        if col in cols:
            return fe


def get_tdac_range(fe):
    return 0, 32, 32, 1


class CROCv1(ITkPixV1):
    '''
    Main class for CROCv1 chip
    '''

    flavor_cols = FLAVOR_COLS

    def __init__(self, bdaq, chip_sn='0x0000', chip_id=0, receiver='rx0', config=None):
        self.log = logger.setup_derived_logger('CROCv1 - ' + chip_sn)
        self.bdaq = bdaq
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if chip_id not in range(16):
            raise ValueError("Invalid Chip ID %r, use integer 0-15" % chip_id)

        self.chip_type = 'CROCv1'
        self.chip_sn = chip_sn
        self.chip_id = chip_id
        self.receiver = receiver

        if config is None or len(config) == 0:
            self.log.warning("No explicit configuration supplied. Using 'CROCv1_default.cfg.yaml'!")
            with open(os.path.join(os.path.dirname(__file__), 'chips', 'CROCv1_default.cfg.yaml'), 'r') as f:
                self.configuration = yaml.full_load(f)
        elif isinstance(config, dict):
            self.configuration = config
        elif isinstance(config, str) and os.path.isfile(config):
            with open(config) as f:
                self.configuration = yaml.full_load(f)
        else:
            raise TypeError('Supplied config has unknown format!')

        self.registers = RegisterObject(self, 'ITkPixV1_registers.yaml')

        masks = {'enable': {'default': False},
                 'injection': {'default': False},
                 'hitbus': {'default': False},
                 'tdac': {'default': 0},
                 'lin_gain_sel': {'default': True},
                 'injection_delay': {'default': 351}}
        self.masks = ITkPixV1MaskObject(self, masks, (432, 336))

        # Load disabled pixels from chip config
        if 'disable' in self.configuration.keys():
            for pix in self.configuration['disable']:
                self.masks.disable_mask[pix[0], pix[1]] = False

        self.calibration = ITkPixV1Calibration(self.configuration['calibration'])


if __name__ == '__main__':
    CROCv1_chip = CROCv1()
    CROCv1_chip.init()
