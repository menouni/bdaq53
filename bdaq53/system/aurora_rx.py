#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from basil.HL.RegisterHardwareLayer import RegisterHardwareLayer
from bdaq53.system import logger


class aurora_rx(RegisterHardwareLayer):
    '''
    '''

    _registers = {'RESET': {'descr': {'addr': 0, 'size': 8, 'properties': ['writeonly']}},
                  'VERSION': {'descr': {'addr': 0, 'size': 8, 'properties': ['ro']}},

                  'EN': {'descr': {'addr': 2, 'size': 1, 'offset': 0}},
                  'RX_READY': {'descr': {'addr': 2, 'size': 1, 'offset': 1, 'properties': ['readonly']}},
                  'RX_LANE_UP': {'descr': {'addr': 2, 'size': 1, 'offset': 2, 'properties': ['readonly']}},
                  'PLL_LOCKED': {'descr': {'addr': 2, 'size': 1, 'offset': 3, 'properties': ['readonly']}},
                  'RX_HARD_ERROR': {'descr': {'addr': 2, 'size': 1, 'offset': 4, 'properties': ['readonly']}},
                  'RX_SOFT_ERROR': {'descr': {'addr': 2, 'size': 1, 'offset': 5, 'properties': ['readonly']}},
                  'USER_K_FILTER_MODE': {'descr': {'addr': 2, 'size': 2, 'offset': 6}},

                  'LOST_COUNT': {'descr': {'addr': 3, 'size': 8, 'properties': ['ro']}},

                  'USER_K_FILTER_MASK_1': {'descr': {'addr': 4, 'size': 8}},
                  'USER_K_FILTER_MASK_2': {'descr': {'addr': 5, 'size': 8}},
                  'USER_K_FILTER_MASK_3': {'descr': {'addr': 6, 'size': 8}},

                  'RESET_COUNTERS': {'descr': {'addr': 7, 'size': 1, 'offset': 0}},
                  'RESET_LOGIC': {'descr': {'addr': 7, 'size': 1, 'offset': 1}},
                  'GTX_TX_MODE': {'descr': {'addr': 7, 'size': 1, 'offset': 3}},

                  'FRAME_COUNTER': {'descr': {'addr': 8, 'size': 32}},
                  'SOFT_ERROR_COUNTER': {'descr': {'addr': 12, 'size': 8}},
                  'HARD_ERROR_COUNTER': {'descr': {'addr': 13, 'size': 8}},
                  'RX_LANES': {'descr': {'addr': 14, 'size': 4, 'offset': 4}}
                  }
    _require_version = "==4"

    def __init__(self, intf, conf):
        self.log = logger.setup_derived_logger('AuroraRX')

        super(aurora_rx, self).__init__(intf, conf)

    def reset(self):
        '''Soft reset the module'''
        self.RESET = 0

    def reset_counters(self):
        '''Resets the soft/hard error counters'''
        self.RESET_COUNTERS = True
        self.RESET_COUNTERS = False

    def reset_logic(self):
        '''Resets only the logic and the FIFOs'''
        self.RESET_LOGIC = True
        self.RESET_LOGIC = False

    def set_en(self, value):
        self.EN = value

    def get_en(self):
        return self.EN

    def get_rx_ready(self):
        '''Aurora link established'''
        return self.RX_READY

    def get_pll_locked(self):
        '''Aurora PLL locked'''
        return self.PLL_LOCKED

    def get_lost_count(self):
        '''Lost data due to RX FIFO overflow'''
        return self.LOST_COUNT

    def get_SOFT_ERROR_COUNTER(self):
        '''Aurora soft errors'''
        return self.SOFT_ERROR_COUNTER

    def get_HARD_ERROR_COUNTER(self):
        '''Aurora hard errors'''
        return self.HARD_ERROR_COUNTER

    def set_USER_K_FILTER_MASK(self, mask, value):
        ''' Define up to 3 user_k patterns, which are rejected by the FPGA '''
        if mask == 1:
            self.USER_K_FILTER_MASK_1 = value
        elif mask == 2:
            self.USER_K_FILTER_MASK_2 = value
        elif mask == 3:
            self.USER_K_FILTER_MASK_3 = value
        else:
            self.log.error("USER_K_FILTER_MASK: Parameters: mask_number[1,2], value[byte]")

    def get_USER_K_FILTER_MASK(self):
        return self.USER_K_FILTER_MASK

    def set_USER_K_FILTER_MODE(self, value='block'):
        ''' Configure the USER_K filter. It can either block, filter(following the set filters) ot pass the register/monitor USER_K data'''
        _user_k_mode = {
            'block': 0b00,
            'filter': 0b01,
            'pass': 0b10}
        if value in _user_k_mode:
            self.USER_K_FILTER_MODE = _user_k_mode[value]
            self.log.debug("USER_K filter mode: %s" % value)
        else:
            self.log.error("USER_K filter mode: '%s' not allowed. Parameters: 'block', 'filter' or 'pass'" % value)

    def get_USER_K_FILTER_MODE(self):
        return self.USER_K_FILTER_MODE

    def get_frame_count(self):
        return self.FRAME_COUNTER

    def set_GTX_TX_MODE(self, value):
        '''
            Set operation mode for the GTX transmitter
            - CMD:     Command data is sent via GTX
            - CLK640:  640 MHz clock output
            Modes can be changed at any time after get_pll_locked==1
        '''
        if value == 'CMD':
            self.GTX_TX_MODE = 0
            self.log.debug("GTX TX MODE: Command data")
        elif value == 'CLK640':
            self.GTX_TX_MODE = 1
            self.log.debug("GTX TX MODE: 640 MHz clock")
        else:
            self.log.error("GTX TX MODE: parameter out of range")

    def get_rx_config(self):
        return self.RX_LANES
