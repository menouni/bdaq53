#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This meta script simply performs threshold scans to obtain
    the optimal TDAC value per pixel to get as close to the target
    threshold as possible. The result is a TDAC mask file.
'''

import os
import numpy as np
import tables as tb
from copy import deepcopy

from bdaq53.system import logger, scan_base
from bdaq53.chips import rd53a
from bdaq53.scans.scan_threshold import ThresholdScan


scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'use_maskfile': False,

    # Final threshold scan parameters
    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 700,
    'VCAL_HIGH_step': 10
}


class MetaTDACTuning(object):
    def __init__(self, scan_config):
        self.log = logger.setup_derived_logger('MetaTDACTuning')

        self._maskfiles = []

        # Need ScanBase __init__ to obtain multi-chip/multi-module variable lists
        # TODO: temporary turn off logging here
        tmp_scan = ThresholdScan()
        self._output_directories = tmp_scan._output_directories_per_scan
        self._scan_configuration_per_scan = tmp_scan._scan_configuration_per_scan
        for scan_config in self._scan_configuration_per_scan:
            self._maskfiles.append(scan_config['maskfile'])
        del tmp_scan

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            self.log.error(exc_value)
            self.log.error('Scan failed!')

    def scan(self, start_column=128, stop_column=264, start_row=0, stop_row=192, VCAL_MED=500, VCAL_HIGH_start=500, VCAL_HIGH_stop=700, VCAL_HIGH_step=10, use_maskfile=False):
        self.threshold_scan_config = {'start_column': start_column, 'stop_column': stop_column, 'start_row': start_row, 'stop_row': stop_row,
                                      'VCAL_MED': VCAL_MED, 'VCAL_HIGH_start': VCAL_HIGH_start, 'VCAL_HIGH_stop': VCAL_HIGH_stop, 'VCAL_HIGH_step': VCAL_HIGH_step}

        if not use_maskfile:
            self.threshold_scan_config.update({'maskfile': None})

        flavor = rd53a.get_flavor(stop_column - 1)

        for scan_config in self._scan_configuration_per_scan:
            scan_config.update(deepcopy(self.threshold_scan_config))

        self._tdacs = []
        self._retune = []

        for output_dir, maskfile, threshold_scan_config in zip(self._output_directories, self._maskfiles, self._scan_configuration_per_scan):
            # Prepare starting TDAC mask
            retune = False
            tdac = np.zeros((400, 192), dtype=int)

            # If there's a maskfile, load TDAC mask from there
            if use_maskfile and (maskfile is not None):
                if maskfile == 'auto':
                    maskfile = scan_base.get_latest_maskfile(output_dir)
                try:
                    with tb.open_file(maskfile) as in_file:
                        self.log.info('Loading TDAC from file: %s ' % (maskfile))
                        tdac = in_file.root.masks.tdac[:]
                        if flavor == 'LIN' and np.all(tdac[start_column:stop_column, start_row:stop_row] == 7):
                            retune = False
                        elif flavor == 'DIFF' and np.all(tdac[start_column:stop_column, start_row:stop_row] == 0):
                            retune = False
                        else:
                            retune = True
                except Exception:
                    pass

            self._retune.append(retune)

            if not retune and flavor == 'LIN':  # Cannot start with all TDACs at 7, have to put half to 7 half to 8
                tdac[start_column:stop_column, start_row:stop_row] = 7
                tdac[start_column:stop_column, start_row:stop_row:2] = 8

            threshold_scan_config['TDAC'] = tdac
            self._tdacs.append(tdac)

        retune = self._retune[0]
        for t_retune in self._retune:
            if t_retune != retune:
                raise RuntimeError("Can only retune *all* chips at the same time or *not* retune *all* chips at the same time.")

        # Prepare stepsize
        th_scan = [8, 4, 2, 1, 1]  # For DIFF
        if flavor == 'LIN':
            th_scan = [4, 2, 1, 1]  # For LIN

        if retune:
            th_scan = [1, 1, 1, 1]  # 4 adjustment steps by 1 TDAC in case a mask was provided

        # Main scan loop
        for stepsize in th_scan:
            self._tdacs, _, _, _ = self.correct(stepsize, self._tdacs)

        self.log.info('Final adjustments...')
        for threshold_scan_config in self._scan_configuration_per_scan:
            threshold_scan_config['VCAL_HIGH_step'] = 5

        self._tdacs, self._means, self._ths, self._tdacs0 = self.correct(1, self._tdacs)
        self._tdacs, self._means, self._ths1, self._tdacs1 = self.correct(1, self._tdacs)

        for tdac, mean, th, tdac0, th1, tdac1 in zip(self._tdacs, self._means, self._ths, self._tdacs0, self._ths1, self._tdacs1):
            corr = np.abs(th1 - mean) > np.abs(th - mean)
            for col in range(start_column, stop_column):
                for row in range(start_row, stop_row):
                    if corr[col, row]:
                        tdac[col, row] = tdac0[col, row]
                    else:
                        tdac[col, row] = tdac1[col, row]

        # Final threshold scan
        for threshold_scan_config, tdac in zip(self._scan_configuration_per_scan, self._tdacs):
            threshold_scan_config['TDAC'] = tdac

        with ThresholdScan(record_chip_status=False) as scan:
            scan._scan_configuration_per_scan = self._scan_configuration_per_scan
            scan.scan()
            scan.analyze()
            scan.save_all_maskfiles()

        self.log.success('Tuning finished!')

    def correct(self, stepsize, tdac_list):
        start_column = self.threshold_scan_config.get('start_column')
        stop_column = self.threshold_scan_config.get('stop_column')
        start_row = self.threshold_scan_config.get('start_row')
        stop_row = self.threshold_scan_config.get('stop_row')
        flavor = rd53a.get_flavor(stop_column - 1)

        # Run threshold scan
        for threshold_scan_config, tdac in zip(self._scan_configuration_per_scan, tdac_list):
            threshold_scan_config['TDAC'] = tdac

        with ThresholdScan(record_chip_status=False) as scan:
            threshold_scan_run_name = scan.run_name
            scan._scan_configuration_per_scan = self._scan_configuration_per_scan
            scan.scan()
            scan.analyze()

        # Lists with return values for each chip
        tdac_return = []
        mean_return = []
        thr_map_return = []
        tdac_before_return = []

        for output_dir, tdac in zip(self._output_directories, tdac_list):
            output_filename = os.path.join(output_dir, threshold_scan_run_name)

            # Analyze results
            with tb.open_file(output_filename + '_interpreted.h5') as in_file:
                thr_map = in_file.root.ThresholdMap[:]

                thr_in_range = thr_map[start_column:stop_column, start_row:stop_row]    # Only valid entries of the full threhsold map
                mean = np.median(thr_in_range[thr_in_range > 1])
                # Prepare settings for next threshold scan
                thr_max = np.max(thr_in_range[thr_in_range < (self.threshold_scan_config.get('VCAL_HIGH_stop') - self.threshold_scan_config.get('VCAL_HIGH_start'))]) + 25
                self.threshold_scan_config['VCAL_HIGH_stop'] = self.threshold_scan_config.get('VCAL_HIGH_start') + int(thr_max)

                tdac_before = tdac.copy()
                for col in range(start_column, stop_column):
                    for row in range(start_row, stop_row):
                        if flavor == 'LIN':
                            if thr_map[col, row] > mean:
                                tdac[col, row] = min(tdac[col, row] + stepsize, 15)
                            else:
                                tdac[col, row] = max(tdac[col, row] - stepsize, 0)
                        else:
                            if thr_map[col, row] > mean:
                                tdac[col, row] = max(tdac[col, row] - stepsize, -15)
                            else:
                                tdac[col, row] = min(tdac[col, row] + stepsize, 15)

                # Log current status
                if flavor == 'LIN':
                    bc = np.bincount(tdac[start_column:stop_column, start_row:stop_row].flatten(), minlength=16)
                else:
                    bc = np.bincount(tdac[start_column:stop_column, start_row:stop_row].flatten() + 15, minlength=31)
                np.set_printoptions(linewidth=200)
                self.log.info('Mean threshold = %f, Maximum threshold = %f ,TDAC distribution %s' % (mean, thr_max, str(bc)))

                tdac_return.append(tdac), mean_return.append(mean), thr_map_return.append(thr_map), tdac_before_return.append(tdac_before)

        return tdac_return, mean_return, thr_map_return, tdac_before_return


if __name__ == '__main__':
    with MetaTDACTuning(scan_config=scan_configuration) as tuning:
        tuning.scan(**scan_configuration)
