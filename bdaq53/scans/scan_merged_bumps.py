#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This scan identifies merged bump bonds by performing an analog scan using a crosstalk injection pattern
    and small injected charge (compared to the previous tuning). The chip has to be tuned (without sensor HV)
    prior to this scan, e.g. to 3000e. The DVCAL value of the tuning must be entered in the dictionary below.
    In order to prevent real crosstalk, the sensor has to be reverse biased for this scan.
'''

import tables as tb
import numpy as np

from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    'VCAL_MED': 500,
    'DVCAL_tuned': 273  # 3000e
}


class MergedBumpsScan(ScanBase):
    scan_id = 'merged_bumps_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, VCAL_MED=500, DVCAL_tuned=273, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        VCAL_MED : int
            VCAL_MED DAC value.
        DVCAL_tuned : int
            Delta VCAL value from previous tuning.
        '''

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

        self.data.VCAL_HIGH = VCAL_MED + 2 * DVCAL_tuned

        self.chip.setup_analog_injection(vcal_high=self.data.VCAL_HIGH, vcal_med=VCAL_MED)

    def _scan(self, n_injections=100, **_):
        '''
        Analog scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''
        pbar = tqdm(total=self.chip.masks.get_mask_steps(), unit=' Mask steps')
        with self.readout():
            for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], pattern='ring_injection'):
                if not fe == 'skipped' and fe == 'SYNC':
                    self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections, latency=122, wait_cycles=400)
                elif not fe == 'skipped':
                    self.chip.inject_analog_single(repetitions=n_injections, latency=122, wait_cycles=400)
                pbar.update(1)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            n_injections = a.scan_config['n_injections']

        with tb.open_file(self.output_filename + '_interpreted.h5', 'r+') as in_file:
            hist_occ = in_file.root.HistOcc[:].reshape((400, 192))

            merged = hist_occ > 0.5 * n_injections

            merged_bumps = np.zeros(shape=(400, 192))
            merged_bumps[merged] = 1

            in_file.create_carray(in_file.root, name='HistMergedBumps', title='Merged Bump Bonds', obj=merged_bumps,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

                p._plot_boolean_map(hist=merged_bumps, z_label='Merged', title='Merged Bump Bonds Map', suffix='merged_bumps_map')


if __name__ == '__main__':
    with MergedBumpsScan(scan_config=scan_configuration) as scan:
        scan.start()
