#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan reads out one pixel while injecting to neighbouring ones
    using different injection mask patterns to check for crosstalk.
    The working principle is the same as in a threshold scan.
'''

import numpy as np

from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject, get_scan_loop_mask_steps
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'VCAL_MED': 0,
    'VCAL_HIGH_start': 0,
    'VCAL_HIGH_stop': 4096,
    'VCAL_HIGH_step': 102,

    # Type of injection
    'injection_type': 'cross_injection'
    # Use one from:
    # 'vertical_injection'  : Inject into up and down neighbors
    # 'horizontal_injection': Inject into left and right neighbors
    # 'cross_injection'     : Inject into up, down, left and right neighbors
    # 'corner_injection'    : inject into the 4 corner pixels
    # 'odd-left_injection'  : Inject into left neighbor if it is located in odd row
    # 'odd-right_injection' : Inject into right neighbor if it is located in odd row
    # 'even-left_injection' : Inject into left neighbor if it is located in even row
    # 'even-right_injection': Inject into right neighbor if it is located in even row
}


class CrosstalkScan(ScanBase):
    scan_id = "crosstalk_scan"

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

    def _scan(self, injection_type='cross_injection', n_injections=100, VCAL_MED=0, VCAL_HIGH_start=0, VCAL_HIGH_stop=4096, VCAL_HIGH_step=102, **_):
        '''
        Crosstalk scan main loop

        Parameters
        ----------
        injection_type: str
            injection pattern to use. Refer to wiki.
        n_injections : int
            Number of injections
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''

        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)

        self.log.info('Using {0} pattern.'.format(injection_type))

        pbar = tqdm(total=get_scan_loop_mask_steps(scan=self, pattern=injection_type) * len(vcal_high_range), unit=' Mask steps')
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
            with self.readout(scan_param_id=scan_param_id):
                shift_and_inject(scan=self, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id, pattern=injection_type, skip_empty=False, cache=True)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            mean_thr = np.median(a.threshold_map[np.nonzero(a.threshold_map)])
            if np.isfinite(mean_thr):
                self.log.success('Mean threshold is {0} [Delta VCAL]'.format(int(mean_thr)))
            else:
                self.log.error('Mean threshold could not be determined!')

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


if __name__ == '__main__':
    with CrosstalkScan(scan_config=scan_configuration) as scan:
        scan.start()
